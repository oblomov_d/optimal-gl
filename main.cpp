//internal includes
#include "common.h"
#include "ShaderProgram.h"
#include "LiteMath.h"

//External dependencies
#define GLFW_DLL
#include <GLFW/glfw3.h>
#include <random>

static GLsizei WIDTH = 512, HEIGHT = 512; //размеры окна
// static GLsizei WIDTH = 128, HEIGHT = 128; //размеры окна

using namespace LiteMath;

const int KEY_COUNT = 10;

float3 g_camPos(0, 0, 5);
float3 default_pos(0, 0, 5);
float  cam_rot[2] = {0,0};
float default_rot[2] = {0, 0};
float main_scale = 1.0f;
bool camera_lock = true;
float2 mpos;
// W A S D R F 0 L U I
char wasd[KEY_COUNT] = {};
int change_ind = -1;
bool mouse_pressed = false;
float2 a1(0.25, 0.25), 
       a2(0.25, 0.75), 
       q1(0.75, 0.25), 
       q2(0.75, 0.75);

float2 ps[5];

const float min_r = 0.05;

void windowResize(GLFWwindow* window, int width, int height){
  WIDTH  = width;
  HEIGHT = height;
}

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
    if (button == GLFW_MOUSE_BUTTON_LEFT){
        if (action == GLFW_PRESS){
            mouse_pressed = true; 

            for (int i = 0; i < 5; i++){
                float len = length(ps[i] - mpos);
                if (len < min_r) {
                    change_ind = i;
                    break;
                }
            }
        } else if (action == GLFW_RELEASE){
            mouse_pressed = false;
            change_ind = -1;
        } 
    }
}

static void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos){
    mpos.x = xpos / WIDTH;
    mpos.y = ypos / HEIGHT;
    if (change_ind != -1) {
        ps[change_ind] = mpos;
    }
}

void keyPress(GLFWwindow *window, int key, int scancode, int action, int mods){
  int keys[KEY_COUNT] = 
                      {GLFW_KEY_W, GLFW_KEY_A, GLFW_KEY_S, GLFW_KEY_D, 
                       GLFW_KEY_R, GLFW_KEY_F, GLFW_KEY_0, GLFW_KEY_L,
                       GLFW_KEY_U, GLFW_KEY_I
                      };
  for (int i = 0; i < KEY_COUNT; i++){
    if (key == keys[i]){
        //
        //
      if (action == GLFW_PRESS)
        wasd[i] = 1;
      else if (action == GLFW_RELEASE)
        wasd[i] = 0;
      break;
    }
  }
  if (action == GLFW_RELEASE)
    if (key == GLFW_KEY_ESCAPE)
      glfwSetWindowShouldClose(window, 1);
    else if (key == GLFW_KEY_L)
      camera_lock = !camera_lock;

}

void updatePos(){
  float speed = 0.1;
  float3 dir;
  float dx, dy, dz;
  dz = (wasd[0] - wasd[2])*speed;
  dx = (wasd[1] - wasd[3])*speed;
  dy = (wasd[5] - wasd[4])*speed;
  dir = float3(dx, 0, dz);
  main_scale += (wasd[9] - wasd[8])*speed;

  float4x4 camRotMatrix   = mul(rotate_Y_4x4(-cam_rot[1]), rotate_X_4x4(+cam_rot[0]));
  g_camPos = g_camPos - mul4x3(camRotMatrix, dir);
  g_camPos = g_camPos - float3(0, dy, 0);

  // 0 IS PRESSED:
  if (wasd[6]){
    g_camPos = default_pos;
    cam_rot[0] = default_rot[0];
    cam_rot[1] = default_rot[1];
  }
}

int initGL()
{
	int res = 0;
	//грузим функции opengl через glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize OpenGL context" << std::endl;
		return -1;
	}

	std::cout << "Vendor: "   << glGetString(GL_VENDOR) << std::endl;
	std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "Version: "  << glGetString(GL_VERSION) << std::endl;
	std::cout << "GLSL: "     << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	return 0;
}

int main(int argc, char** argv){
	if(!glfwInit())
        return -1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); 
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); 
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); 
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); 

     GLFWwindow*  window = glfwCreateWindow(WIDTH, HEIGHT, "xd",  nullptr, nullptr);
	if (window == nullptr){
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

    glfwSetCursorPosCallback(window, cursor_pos_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetWindowSizeCallback(window, windowResize);
    glfwSetKeyCallback(window, keyPress);

	glfwMakeContextCurrent(window); 
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    ps[0] = float2(0.25, 0.25);
    ps[1] = float2(0.25, 0.75);
    ps[2] = float2(0.75, 0.25);
    ps[3] = float2(0.75, 0.75);
    ps[4] = float2(0.50, 0.50);

	if(initGL() != 0) 
		return -1;
	
  //Reset any OpenGL errors which could be present for some reason
	GLenum gl_error = glGetError();
	while (gl_error != GL_NO_ERROR)
		gl_error = glGetError();

	//создание шейдерной программы из двух файлов с исходниками шейдеров
	//используется класс-обертка ShaderProgram
	std::unordered_map<GLenum, std::string> shaders;
	shaders[GL_VERTEX_SHADER]   = "vertex.glsl";
	shaders[GL_FRAGMENT_SHADER] = "fragment.glsl";
	ShaderProgram program(shaders); GL_CHECK_ERRORS;

  glfwSwapInterval(1); // force 60 frames per second
  
  //Создаем и загружаем геометрию поверхности
  //
  GLuint g_vertexBufferObject;
  GLuint g_vertexArrayObject;
  {
 
    float quadPos[] =
    {
      -1.0f,  1.0f,	// v0 - top left corner
      -1.0f, -1.0f,	// v1 - bottom left corner
      1.0f,  1.0f,	// v2 - top right corner
      1.0f, -1.0f	  // v3 - bottom right corner
    };

    g_vertexBufferObject = 0;
    GLuint vertexLocation = 0; // simple layout, assume have only positions at location = 0

    glGenBuffers(1, &g_vertexBufferObject);                                                        GL_CHECK_ERRORS;
    glBindBuffer(GL_ARRAY_BUFFER, g_vertexBufferObject);                                           GL_CHECK_ERRORS;
    glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), (GLfloat*)quadPos, GL_STATIC_DRAW);     GL_CHECK_ERRORS;

    glGenVertexArrays(1, &g_vertexArrayObject);                                                    GL_CHECK_ERRORS;
    glBindVertexArray(g_vertexArrayObject);                                                        GL_CHECK_ERRORS;

    glBindBuffer(GL_ARRAY_BUFFER, g_vertexBufferObject);                                           GL_CHECK_ERRORS;
    glEnableVertexAttribArray(vertexLocation);                                                     GL_CHECK_ERRORS;
    glVertexAttribPointer(vertexLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);                            GL_CHECK_ERRORS;

    glBindVertexArray(0);
  }

  float time;

	//цикл обработки сообщений и отрисовки сцены каждый кадр
    while (!glfwWindowShouldClose(window)){
	    glfwPollEvents();
        updatePos();
		    
        time = glfwGetTime();
        //очищаем экран каждый кадр
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);               GL_CHECK_ERRORS;
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;

        program.StartUseShader();                           GL_CHECK_ERRORS;

        float4x4 camRotMatrix   = mul(rotate_Y_4x4(-cam_rot[1]), rotate_X_4x4(+cam_rot[0]));
        float4x4 camTransMatrix = translate4x4(g_camPos);
        float4x4 rayMatrix      = mul(camTransMatrix, camRotMatrix);
        program.SetUniform("ps", &ps[0], 5);

        glViewport  (0, 0, WIDTH, HEIGHT);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear     (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        glBindVertexArray(g_vertexArrayObject); GL_CHECK_ERRORS;
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);  GL_CHECK_ERRORS;  // The last parameter of glDrawArrays is equal to VS invocations
        
        program.StopUseShader();

		glfwSwapBuffers(window); 
	}

	//очищаем vboи vao перед закрытием программы
  //
	glDeleteVertexArrays(1, &g_vertexArrayObject);
  glDeleteBuffers(1,      &g_vertexBufferObject);

	glfwTerminate();

	return 0;
}
