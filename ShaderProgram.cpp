#include "ShaderProgram.h"

ShaderProgram::ShaderProgram(const std::unordered_map<GLenum, std::string> &inputShaders)
{

  shaderProgram = glCreateProgram();

  if (inputShaders.find(GL_VERTEX_SHADER) != inputShaders.end())
  {
    shaderObjects[GL_VERTEX_SHADER] = LoadShaderObject(GL_VERTEX_SHADER, inputShaders.at(GL_VERTEX_SHADER));
    glAttachShader(shaderProgram, shaderObjects[GL_VERTEX_SHADER]);
  }

  if (inputShaders.find(GL_FRAGMENT_SHADER) != inputShaders.end())
  {
    shaderObjects[GL_FRAGMENT_SHADER] = LoadShaderObject(GL_FRAGMENT_SHADER, inputShaders.at(GL_FRAGMENT_SHADER));
    glAttachShader(shaderProgram, shaderObjects[GL_FRAGMENT_SHADER]);
  }
  if (inputShaders.find(GL_GEOMETRY_SHADER) != inputShaders.end())
  {
    shaderObjects[GL_GEOMETRY_SHADER] = LoadShaderObject(GL_GEOMETRY_SHADER, inputShaders.at(GL_GEOMETRY_SHADER));
    glAttachShader(shaderProgram, shaderObjects[GL_GEOMETRY_SHADER]);
  }
  if (inputShaders.find(GL_TESS_CONTROL_SHADER) != inputShaders.end())
  {
    shaderObjects[GL_TESS_CONTROL_SHADER] = LoadShaderObject(GL_TESS_CONTROL_SHADER,
      inputShaders.at(GL_TESS_CONTROL_SHADER));
    glAttachShader(shaderProgram, shaderObjects[GL_TESS_CONTROL_SHADER]);
  }
  if (inputShaders.find(GL_TESS_EVALUATION_SHADER) != inputShaders.end())
  {
    shaderObjects[GL_TESS_EVALUATION_SHADER] = LoadShaderObject(GL_TESS_EVALUATION_SHADER,
      inputShaders.at(GL_TESS_EVALUATION_SHADER));
    glAttachShader(shaderProgram, shaderObjects[GL_TESS_EVALUATION_SHADER]);
  }
  if (inputShaders.find(GL_COMPUTE_SHADER) != inputShaders.end())
  {
    shaderObjects[GL_COMPUTE_SHADER] = LoadShaderObject(GL_COMPUTE_SHADER, inputShaders.at(GL_COMPUTE_SHADER));
    glAttachShader(shaderProgram, shaderObjects[GL_COMPUTE_SHADER]);
  }

  glLinkProgram(shaderProgram);

  GLint linkStatus;


  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linkStatus);
  if (linkStatus != GL_TRUE)
  {
    GLchar infoLog[512];
    glGetProgramInfoLog(shaderProgram, 512, nullptr, infoLog);
    std::cerr << "Shader program linking failed\n" << infoLog << std::endl;
    shaderProgram = 0;
  }

}


void ShaderProgram::Release()
{
  if (shaderObjects.find(GL_VERTEX_SHADER) != shaderObjects.end())
  {
    glDetachShader(shaderProgram, shaderObjects[GL_VERTEX_SHADER]);
    glDeleteShader(shaderObjects[GL_VERTEX_SHADER]);
  }

  if (shaderObjects.find(GL_FRAGMENT_SHADER) != shaderObjects.end())
  {
    glDetachShader(shaderProgram, shaderObjects[GL_FRAGMENT_SHADER]);
    glDeleteShader(shaderObjects[GL_FRAGMENT_SHADER]);
  }

  if (shaderObjects.find(GL_GEOMETRY_SHADER) != shaderObjects.end())
  {
    glDetachShader(shaderProgram, shaderObjects[GL_GEOMETRY_SHADER]);
    glDeleteShader(shaderObjects[GL_GEOMETRY_SHADER]);
  }

  if (shaderObjects.find(GL_TESS_CONTROL_SHADER) != shaderObjects.end())
  {
    glDetachShader(shaderProgram, shaderObjects[GL_TESS_CONTROL_SHADER]);
    glDeleteShader(shaderObjects[GL_TESS_CONTROL_SHADER]);
  }

  if (shaderObjects.find(GL_TESS_EVALUATION_SHADER) != shaderObjects.end())
  {
    glDetachShader(shaderProgram, shaderObjects[GL_TESS_EVALUATION_SHADER]);
    glDeleteShader(shaderObjects[GL_TESS_EVALUATION_SHADER]);
  }
  if (shaderObjects.find(GL_COMPUTE_SHADER) != shaderObjects.end())
  {
    glDetachShader(shaderProgram, shaderObjects[GL_COMPUTE_SHADER]);
    glDeleteShader(shaderObjects[GL_COMPUTE_SHADER]);
  }

  glDeleteProgram(shaderProgram);
}

bool ShaderProgram::reLink()
{
  GLint linked;

  glLinkProgram(shaderProgram);
  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linked);

  if (!linked)
  {
    GLint logLength, charsWritten;
    glGetProgramiv(this->shaderProgram, GL_INFO_LOG_LENGTH, &logLength);

    auto log = new char[logLength];
    glGetProgramInfoLog(this->shaderProgram, logLength, &charsWritten, log);

    std::cerr << "Shader program link error: " << std::endl << log << std::endl;

    delete[] log;
    shaderProgram = 0;
    return false;
  }

  return true;
}


GLuint ShaderProgram::LoadShaderObject(GLenum type, const std::string &filename)
{
  std::ifstream fs(filename);

  if (!fs.is_open())
  {
    std::cerr << "ERROR: Could not read shader from " << filename << std::endl;
    return 0;
  }

  std::string shaderText((std::istreambuf_iterator<char>(fs)), std::istreambuf_iterator<char>());

  GLuint newShaderObject = glCreateShader(type);

  const char *shaderSrc = shaderText.c_str();
  glShaderSource(newShaderObject, 1, &shaderSrc, nullptr);

  glCompileShader(newShaderObject);

  GLint compileStatus;
  glGetShaderiv(newShaderObject, GL_COMPILE_STATUS, &compileStatus);

  if (compileStatus != GL_TRUE)
  {
    GLchar infoLog[512];
    glGetShaderInfoLog(newShaderObject, 512, nullptr, infoLog);
    std::cerr << "Shader compilation failed : " << std::endl << infoLog << std::endl;
    return 0;
  }

  return newShaderObject;
}

void ShaderProgram::StartUseShader() const
{
  glUseProgram(shaderProgram);
}

void ShaderProgram::StopUseShader() const
{
  glUseProgram(0);
}

void ShaderProgram::SetUniform(const std::string &location, int value) const
{
  GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
  if (uniformLocation == -1)
  {
    std::cerr << "Uniform  " << location << " not found" << std::endl;
    return;
  }
  glUniform1i(uniformLocation, value);
}

void ShaderProgram::SetUniform(const std::string &location, unsigned int value) const
{
  GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
  if (uniformLocation == -1)
  {
    std::cerr << "Uniform  " << location << " not found" << std::endl;
    return;
  }
  glUniform1ui(uniformLocation, value);
}

void ShaderProgram::SetUniform(const std::string &location, float value) const
{
  GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
  if (uniformLocation == -1)
  {
    std::cerr << "Uniform  " << location << " not found" << std::endl;
    return;
  }
  glUniform1f(uniformLocation, value);
}

void ShaderProgram::SetUniform(const std::string &location, float2 &val) const {
  GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
  if (uniformLocation == -1)
  {
    std::cerr << "Uniform  " << location << " not found" << std::endl;
    return;
  }
  glUniform2f(uniformLocation, val.x, val.y);
}

void ShaderProgram::SetUniform(const std::string &location, float2 *val, int cnt) const {
  GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
  if (uniformLocation == -1)
  {
    std::cerr << "Uniform  " << location << " not found" << std::endl;
    return;
  }
  glUniform2fv(uniformLocation, cnt, reinterpret_cast<float*>(val));
}

void ShaderProgram::SetUniform(const std::string &location, LiteMath::float4x4 a_mat) const
{
  GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
  if (uniformLocation == -1)
  {
    std::cerr << "Uniform  " << location << " not found" << std::endl;
    return;
  }

  glUniformMatrix4fv(uniformLocation, 1, true, a_mat.L());
}

void ShaderProgram::SetUniform(const std::string &location, double value) const
{
  GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
  if (uniformLocation == -1)
  {
    std::cerr << "Uniform  " << location << " not found" << std::endl;
    return;
  }
  glUniform1d(uniformLocation, value);
}


void ShaderProgram::SetUniformTexture(const std::string &location, GLuint textureID) const
{
  GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
  if (uniformLocation == -1)
  {
    std::cerr << "Uniform  " << location << " not found" << std::endl;
    return;
  }
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
  glUniform1i(uniformLocation, 0);
}

//DOES NOT FREE MEMORY
unsigned char* getDataBMP(const std::string &filename, unsigned int &width, unsigned int &height){
  printf("Reading image %s\n", filename.c_str());
  unsigned char header[54];
  unsigned int dataPos;
  unsigned int imageSize;
  unsigned char * data;

  FILE * file = fopen(filename.c_str(),"rb");
  if (!file){
    printf("%s could not be opened.", filename.c_str());
    return 0;
  }

  if ( fread(header, 1, 54, file)!=54 ){ 
    printf("Not a correct BMP file 1\n");
    fclose(file);
    return 0;
  }
  if ( header[0]!='B' || header[1]!='M' ){
    printf("Not a correct BMP file 2\n");
    fclose(file);
    return 0;
  }
  if ( *(int*)&(header[0x1E])!=0  )         {printf("Not a correct BMP file 3\n");    fclose(file); return 0;}
  if ( *(int*)&(header[0x1C])!=24 )         {printf("Not a correct BMP file 4\n");    fclose(file); return 0;}

  dataPos    = *(int*)&(header[0x0A]);
  imageSize  = *(int*)&(header[0x22]);
  width      = *(int*)&(header[0x12]);
  height     = *(int*)&(header[0x16]);

  if (imageSize==0)    imageSize=width*height*3; // 3 : one byte for each Red, Green and Blue component
  if (dataPos==0)      dataPos=54; // The BMP header is done that way

  data = new unsigned char [imageSize];

  fread(data,1,imageSize,file);
  // printf("%d\n", (int)data[0]);

  fclose (file);
  return data;
}

GLuint ShaderProgram::LoadBMP(const std::string &filename) const
{
  unsigned int width, height;
  unsigned char* data = getDataBMP(filename, width, height);

  GLuint textureID;
  glGenTextures(1, &textureID);
  
  glBindTexture(GL_TEXTURE_2D, textureID);

  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

  delete [] data;

  // Poor filtering, or ...
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 

  // ... nice trilinear filtering ...
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glGenerateMipmap(GL_TEXTURE_2D);

  return textureID;
}

GLuint ShaderProgram::LoadCubeMapBMP() const
{
  unsigned int width, height;
  unsigned char *x1, *x2, *y1, *y2, *z1, *z2;
  x1 = getDataBMP("x1.bmp", width, height);
  x2 = getDataBMP("x2.bmp", width, height);
  y1 = getDataBMP("y1.bmp", width, height);
  y2 = getDataBMP("y2.bmp", width, height);
  z1 = getDataBMP("z1.bmp", width, height);
  z2 = getDataBMP("z2.bmp", width, height);

  GLuint textureID;

  // glUseProgram(shaderProgram);
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_CUBE_MAP, textureID); GL_CHECK_ERRORS;

  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, x1); GL_CHECK_ERRORS;
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, x2);
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, y1);
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, y2);
  glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, z1);
  glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, z2);
  //TODO 
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // glGenerateMipmap(GL_TEXTURE_CUBE_MAP); GL_CHECK_ERRORS;
  // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); GL_CHECK_ERRORS;
  // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST); GL_CHECK_ERRORS;

  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);


  delete [] x1;
  delete [] x2;
  delete [] y1;
  delete [] y2;
  delete [] z1;
  delete [] z2;

  return textureID;
}
