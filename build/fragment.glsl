#version 330

#define float2 vec2
#define float3 vec3
#define float4 vec4
#define float4x4 mat4
#define float3x3 mat3

in float2 fragmentTexCoord;

layout(location = 0) out vec4 fragColor;

uniform float g_time;
uniform float main_scale;

uniform float2 ps[5];

const float dot_r = 0.03;
const float min_line_r = 0.001;

const float3 blue = float3(0.1, 0.2, 0.9);
const float3 red  = float3(0.9, 0.1, 0.1);
const float3 green = float3(0.1, 0.9, 0.1);

const float3 palette[8] = float3[](
    float3(62.0/255.0, 8/255.0, 76/255.0),
    float3(62.0/255.0, 8/255.0, 76/255.0),
    float3(233/255.0, 25/255.0, 76/255.0),
    float3(233/255.0, 25/255.0, 76/255.0),
    float3(134/255.0, 244/255.0, 238/255.0),
    float3(134/255.0, 244/255.0, 238/255.0),
    float3(2/255.0, 93/255.0, 147/255.0),
    float3(0/255.0, 0/255.0, 0/255.0)
    );

float sdSegment(in vec2 p, in vec2 a, in vec2 b){
    vec2 pa = p-a, ba = b-a;
    float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
    return length( pa - ba*h );
}

void main(void){ 
    float x = fragmentTexCoord.x; 
    float y = 1-fragmentTexCoord.y;
 
    float2 pos = fragmentTexCoord.xy;
    pos.y = y;

    float a1q1 = length(ps[2] - ps[0]);
    float a1q2 = length(ps[3] - ps[0]);
    float a2q1 = length(ps[2] - ps[1]);
    float a2q2 = length(ps[3] - ps[1]);

    float q1b1 = length(ps[4] - ps[2]);
    float q1b2 = length(ps[2] - pos);
    float q2b1 = length(ps[4] - ps[3]);
    float q2b2 = length(ps[3] - pos);

    float a1b1 = length(ps[4] - ps[0]);
    float a1b2 = length(pos - ps[0]);
    float a2b1 = length(ps[4] - ps[1]);
    float a2b2 = length(pos - ps[1]);

    float3 color = float3(0, 0, 0);


    if (a1q1 + a2q2 < a1q2 + a2q1){
    } else {
        color += red;
    }

    if ((q1b1 + q2b2 < q1b2 + q2b1) && (a1b1 + a2b2 < a1b2 + a2b1)) {
        color += green;
    } else {
        color += red;
    }

    if (sdSegment(pos, ps[0], ps[2]) <= min_line_r){
        color = palette[7];
    } else if (sdSegment(pos, ps[1], ps[3]) <= min_line_r){
        color = palette[7];
    } else if (sdSegment(pos, ps[2], ps[4]) <= min_line_r){
        color = palette[7];
    } 
    
    for (int i = 0; i < 5; i++){
        float l = length(pos - ps[i]);
        if (l < dot_r){
            color = palette[i];
        }
    }

    fragColor = float4(color, 1.0);
}

